#include "busquedalocal.h"

// No borrar CliqueMaximaFrontera(g) de la lista de inicialización
CliqueMaximaFronteraBusquedaLocal::CliqueMaximaFronteraBusquedaLocal(const Grafo& g) : CliqueMaximaFrontera(g) {			
	// Agregar comportamiento si hace falta
}

vuint CliqueMaximaFronteraBusquedaLocal::resolver(uint& frontera) {
	uint n = this->grafo.nodos();
	
	vbool mejorSolucion(n);
	mejorSolucion[0] = true;
	
	uint nodosEnSolucionActual = 1;
	uint accion = 3; // 0 borrado, 1 agregado, 2 intercambiado
	uint nodoElegido;
	uint nodoQueSacoSiIntercambio;
	uint fronteraMejorSolucion = 0;
	uint nuevaFrontera = this->grafo.vecindad(0).size();
	
	while (nuevaFrontera > fronteraMejorSolucion) {
		fronteraMejorSolucion = nuevaFrontera;
		if (accion == 2) { // Intercambie
			mejorSolucion[nodoElegido] = true;
			mejorSolucion[nodoQueSacoSiIntercambio] = false;
		}
		else if (accion == 1) { // Agregue
			nodosEnSolucionActual++;
			mejorSolucion[nodoElegido] = true;
		}
		else if (accion == 0) { // Quite
			nodosEnSolucionActual--;
			mejorSolucion[nodoElegido] = false;
		}
		
		// Recorremos todos los nodos para analizar todas opciones que mejoran la solucion.
		for (uint i = 0; i < n; i++) {
			uint fronteraTemp;
			
			if (mejorSolucion[i]) {
				// Si esta en la solucion actual, podemos probar sacarlo...
				fronteraTemp = this->RecalcularFronteraSacando(i, fronteraMejorSolucion, nodosEnSolucionActual);
				
				if (fronteraTemp > nuevaFrontera) {
					nuevaFrontera = fronteraTemp;
					nodoElegido = i;
					accion = 0;
				}
				
				// ... o intercambiarlo con alguno de los no presentes.
				for (uint j = 0; j < n; j ++) {
					if (i != j && !mejorSolucion[j] && this->EsCliqueIntercambiando(i, j, mejorSolucion)) {
						uint fronteraSacando = this->RecalcularFronteraSacando(i, fronteraMejorSolucion, nodosEnSolucionActual);
						fronteraTemp = this->RecalcularFronteraAgregando(j, fronteraSacando, nodosEnSolucionActual - 1);
						
						if (fronteraTemp > nuevaFrontera) {
							nuevaFrontera = fronteraTemp;
							nodoElegido = j;
							nodoQueSacoSiIntercambio = i;
							accion = 2;
						}
					}
				}
			}
			else {
				// Si no esta, probamos agregarlo...
				if (this->EsCliqueAgregando(i, mejorSolucion)) {
					fronteraTemp = this->RecalcularFronteraAgregando(i, fronteraMejorSolucion, nodosEnSolucionActual);
					if (fronteraTemp > nuevaFrontera) {
						nuevaFrontera = fronteraTemp;
						nodoElegido = i;
						accion = 1;
					}
				}
				
				// ... o intercambiarlo con alguno de los existentes.
				for (uint j = 0; j < n; j ++) {
					if (i != j && mejorSolucion[j] && this->EsCliqueIntercambiando(j, i, mejorSolucion)) {
						uint fronteraSacando = this->RecalcularFronteraSacando(j, fronteraMejorSolucion, nodosEnSolucionActual);
						fronteraTemp = this->RecalcularFronteraAgregando(i, fronteraSacando, nodosEnSolucionActual - 1);
						
						if (fronteraTemp > nuevaFrontera) {
							nuevaFrontera = fronteraTemp;
							nodoElegido = i;
							nodoQueSacoSiIntercambio = j;
							accion = 2;
						}
					}
				}
			}
		}
	}
	
	vuint solucion;
	
	// Armo el vector solucion para devolver los presentes.
	for (uint i = 0; i < n; i++) {
		if (mejorSolucion[i]) {
			solucion.push_back(i);
		}
	}
	
	frontera = fronteraMejorSolucion;
	return solucion;
}