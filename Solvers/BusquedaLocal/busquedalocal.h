#ifndef __AED3__LOCAL__
#define __AED3__LOCAL__

#include "../cmf.h"

class CliqueMaximaFronteraBusquedaLocal : public CliqueMaximaFrontera 
{
public:
	CliqueMaximaFronteraBusquedaLocal(const Grafo& g);
	vuint resolver(uint& frontera);			
private:
	// Definir otros m�todos que necesiten
};

#endif
