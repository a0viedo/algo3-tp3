#ifndef __AED3__SOLVER__
#define __AED3__SOLVER__

#include "../Common/common.h"
#include "../Common/grafo.h"

class CliqueMaximaFrontera 
{  
public:
	virtual vuint resolver(uint& frontera) = 0;

protected:
	const Grafo& grafo;
	CliqueMaximaFrontera(const Grafo& g) : grafo(g) {}

	// Devuelve true si 'solucionActual' sigue siendo una clique en caso de agregarse 'nodo'	
	bool EsCliqueAgregando(uint nodo, vbool solucionActual) const
	{
		for (uint i = 0; i < solucionActual.size(); i++)
		{
			if (solucionActual[i] && !this->grafo.sonVecinos(nodo, i))
			{
				return false;						
			}
		}

		return true;
	}

	// Devuelve true si 'solucionActual' sigue siendo una clique en caso de intercambiarse 'nodoAgregado' por 'nodoSacado'	
	bool EsCliqueIntercambiando(uint nodoSacado, uint nodoAgregado, vbool solucionActual) const
	{
		for (uint i = 0; i < solucionActual.size(); i++)
		{
			if (solucionActual[i] && i != nodoSacado && !this->grafo.sonVecinos(nodoAgregado, i))
			{
				return false;						
			}
		}

		return true;
	}

	// Calcula el cardinal de la frontera en caso de que se agregara 'nodo'
	// Asume que agregar el nodo sigue siendo una clique.
	uint RecalcularFronteraAgregando(uint nodo, uint fronteraActual, uint nodosActuales)
	{
		return fronteraActual + this->grafo.vecindad(nodo).size() - (2 * nodosActuales);
	}

	// Calcula el cardinal de la frontera en caso de que se sacara 'nodo'. 
	uint RecalcularFronteraSacando(uint nodo, uint fronteraActual, uint nodosActuales)
	{
		return fronteraActual + (2 * (nodosActuales - 1)) - this->grafo.vecindad(nodo).size();
	}	
};

#endif