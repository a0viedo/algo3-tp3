#include "busquedatabu.h"

CliqueMaximaFronteraBusquedaTabu::CliqueMaximaFronteraBusquedaTabu(const Grafo& g, bool intercambiar, uint limiteSinMejorar, uint duracionTabu) : 
	CliqueMaximaFrontera(g),intercambiar(intercambiar), limiteSinMejorar(limiteSinMejorar), duracionTabu(duracionTabu)
{	
}

vuint CliqueMaximaFronteraBusquedaTabu::resolver(uint& frontera) 
{
	uint n = this->grafo.nodos();

	vbool mejorSolucion(n);
	uint fronteraMejorSolucion = 0;

	mejorSolucion[0] = true;
	fronteraMejorSolucion = this->grafo.vecindad(0).size();
		
	ListaTabu tabu(n);

	vbool solucionActual = mejorSolucion;
	uint fronteraSolucionActual = fronteraMejorSolucion;
	uint nodosEnSolucionActual = 1;

	uint iteracion = 0;
	uint itersSinMejorar = 0;

	while (itersSinMejorar < this->limiteSinMejorar)
	{
		itersSinMejorar++;
		iteracion++;

		uint accion = 3; // 0 borrado, 1 agregado, 2 intercambiado, 3 ninguna accion tomada
		uint nodoAgregado = n;
		uint nodoSacado = n;
		uint nuevaFrontera = 0;

		for (uint i = 0; i < n; i++)
		{
			uint fronteraTemp;
			if (solucionActual[i])
			{
				if (!tabu.EsTabuSacar(i, iteracion))
				{
					fronteraTemp = this->RecalcularFronteraSacando(i, fronteraSolucionActual, nodosEnSolucionActual);

					if (fronteraTemp >= nuevaFrontera)
					{
						nuevaFrontera = fronteraTemp;
						nodoSacado = i;
						accion = 0;
					}
				}

				if (this->intercambiar)
				{
					for (uint j = 0; j < n; j ++)
					{
						if (i != j && !solucionActual[j] && !tabu.EsTabuIntercambiar(i, j, iteracion) && this->EsCliqueIntercambiando(i, j, solucionActual))
						{
							uint fronteraSacando = this->RecalcularFronteraSacando(i, fronteraSolucionActual, nodosEnSolucionActual);
							fronteraTemp = this->RecalcularFronteraAgregando(j, fronteraSacando, nodosEnSolucionActual - 1);

							if (fronteraTemp >= nuevaFrontera)
							{
								nuevaFrontera = fronteraTemp;
								nodoAgregado = j;
								nodoSacado = i;
								accion = 2;
							}
						}
					}
				}
			}
			else
			{
				if (!tabu.EsTabuAgregar(i, iteracion) && this->EsCliqueAgregando(i, solucionActual))
				{
					fronteraTemp = this->RecalcularFronteraAgregando(i, fronteraSolucionActual, nodosEnSolucionActual);
					if (fronteraTemp >= nuevaFrontera)
					{
						nuevaFrontera = fronteraTemp;
						nodoAgregado = i;
						accion = 1;
					}
				}

				if (this->intercambiar)
				{
					for (uint j = 0; j < n; j ++)
					{
						if (i != j && solucionActual[j] && !tabu.EsTabuIntercambiar(j, i, iteracion) && this->EsCliqueIntercambiando(j, i, solucionActual))
						{
							uint fronteraSacando = this->RecalcularFronteraSacando(j, fronteraSolucionActual, nodosEnSolucionActual);
							fronteraTemp = this->RecalcularFronteraAgregando(i, fronteraSacando, nodosEnSolucionActual - 1);

							if (fronteraTemp >= nuevaFrontera)
							{
								nuevaFrontera = fronteraTemp;
								nodoAgregado = i;
								nodoSacado = j;
								accion = 2;
							}
						}
					}
				}
			}
		}

		if (accion != 3)
		{
			fronteraSolucionActual = nuevaFrontera;

			if (accion == 2)
			{
				solucionActual[nodoAgregado] = true;
				solucionActual[nodoSacado] = false;
				tabu.MarcarTabuIntercambiar(nodoAgregado, nodoSacado, iteracion + this->duracionTabu);
			}
			else if (accion == 1)
			{
				nodosEnSolucionActual++;
				solucionActual[nodoAgregado] = true;
				tabu.MarcarTabuAgregar(nodoAgregado, iteracion + this->duracionTabu);
			}
			else
			{
				nodosEnSolucionActual--;
				solucionActual[nodoSacado] = false;
				tabu.MarcarTabuSacar(nodoSacado, iteracion + this->duracionTabu);
			}

			if (fronteraSolucionActual > fronteraMejorSolucion)
			{
				mejorSolucion = solucionActual;
				fronteraMejorSolucion = fronteraSolucionActual;
				itersSinMejorar = 0;
			}
		}
	}

	vuint solucion;

	for (uint i = 0; i < n; i++)
	{
		if (mejorSolucion[i])
		{
			solucion.push_back(i);
		}
	}

	frontera = fronteraMejorSolucion;
	return solucion;
}

CliqueMaximaFronteraBusquedaTabu::ListaTabu::ListaTabu(uint nodos)
{
	this->agregarProhibidos = vuint(nodos);
	this->sacarProhibidos = vuint(nodos);
	this->intercambiarProhibidos = vvuint(nodos);

	for (uint i = 0; i < nodos; i++)
	{
		this->agregarProhibidos[i] = 0;
		this->sacarProhibidos[i] = 0;

		this->intercambiarProhibidos[i] = vuint(nodos);

		for (uint j = 0; j < nodos; j++)
		{
			this->intercambiarProhibidos[i][j] = 0;
		}
	}
}

void CliqueMaximaFronteraBusquedaTabu::ListaTabu::MarcarTabuAgregar(uint nodo, uint expiracion)
{
	this->agregarProhibidos[nodo] = expiracion;	
}

void CliqueMaximaFronteraBusquedaTabu::ListaTabu::MarcarTabuSacar(uint nodo, uint expiracion)
{
	this->sacarProhibidos[nodo] = expiracion;	
}

void CliqueMaximaFronteraBusquedaTabu::ListaTabu::MarcarTabuIntercambiar(uint nodoSacado, uint nodoAgregado, uint expiracion)
{
	this->intercambiarProhibidos[nodoSacado][nodoAgregado] = expiracion;
}

bool CliqueMaximaFronteraBusquedaTabu::ListaTabu::EsTabuAgregar(uint nodo, uint iteracion) const
{
	return this->agregarProhibidos[nodo] > iteracion;	
}

bool CliqueMaximaFronteraBusquedaTabu::ListaTabu::EsTabuSacar(uint nodo, uint iteracion) const
{
	return this->sacarProhibidos[nodo] > iteracion;	
}

bool CliqueMaximaFronteraBusquedaTabu::ListaTabu::EsTabuIntercambiar(uint nodoSacado, uint nodoAgregado, uint iteracion) const
{
	return this->intercambiarProhibidos[nodoSacado][nodoAgregado] > iteracion;	
}