#ifndef __AED3__TABU__
#define __AED3__TABU__

#include "../cmf.h"

class CliqueMaximaFronteraBusquedaTabu : public CliqueMaximaFrontera 
{
public:
	CliqueMaximaFronteraBusquedaTabu(const Grafo& g, bool intercambiar, uint limiteSinMejorar, uint duracionTabu);
	vuint resolver(uint& frontera);			
private:
	bool intercambiar;
	uint limiteSinMejorar;
	uint duracionTabu;

	class ListaTabu 
	{
	public:
		ListaTabu(uint nodos);		

		bool EsTabuAgregar(uint nodo, uint iteracion) const;
		bool EsTabuSacar(uint nodo, uint iteracion) const;
		bool EsTabuIntercambiar(uint nodoSacado, uint nodoAgregado, uint iteracion) const;
		
		void MarcarTabuAgregar(uint nodo, uint expiracion);
		void MarcarTabuSacar(uint nodo, uint expiracion);
		void MarcarTabuIntercambiar(uint nodoSacado, uint nodoAgregado, uint expiracion);
	private:
		vuint agregarProhibidos;
		vuint sacarProhibidos;
		vvuint intercambiarProhibidos;
	};		
};

#endif
