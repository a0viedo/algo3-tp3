#ifndef __AED3__EXACTO__
#define __AED3__EXACTO__

#include "../cmf.h"

class CliqueMaximaFronteraExacto : public CliqueMaximaFrontera 
{
public:
	CliqueMaximaFronteraExacto(const Grafo& g);
	vuint resolver(uint& frontera);
		struct Clique {
	  uint frontera;
	  vuint nodos;
	};
private:

	// Definir otros métodos que necesiten
	uint calcularFrontera(vuint& cliqueNodos, vbool& cliqueBool);
	CliqueMaximaFronteraExacto::Clique backtracking ( vuint clique, vbool cliqueBool, CliqueMaximaFronteraExacto::Clique solucion);
	bool formaClique( uint nodo, vuint& clique);
	void recorrerSusCliques( uint nodo);
	void combinacionesPosibles( int offset, int k);
	vuint obtenerAdyacentes ( vuint& clique, vbool& cliqueBool );
	uint calcularFronteraN ( vuint& clique );
};

#endif
