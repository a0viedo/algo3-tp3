#include "exacto.h"
#include <string>
#include <fstream>
#include <iostream>
using namespace std;
// No borrar CliqueMaximaFrontera(g) de la lista de inicialización
CliqueMaximaFronteraExacto::CliqueMaximaFronteraExacto(const Grafo& g) : CliqueMaximaFrontera(g) 
{			
	// Agregar comportamiento si hace falta
}

//CliqueMaximaFronteraExacto::Clique solucion;
vbool cBool;

void printClique( vuint clique ) {
  cout << "[";
  for(uint i = 0; i < clique.size(); i++) {
    cout << clique[ i ] + 1 << ",";
  }
  cout << "]" << endl;
}

bool CliqueMaximaFronteraExacto::formaClique ( uint nodo, vuint& clique ) {
    vuint vecindad = this->grafo.vecindad(nodo);
    if( vecindad.size() < clique.size() ) {
      return false; // no puede ser vecino de todos
    }
    
    // chequeo si tiene cómo adyacentes a todos los de la clique
    for(uint j = 0; j < clique.size(); j++) {
      bool ese_nodo = false;
      for(uint x = 0; x < vecindad.size(); x++) {
	if( clique[j] == vecindad[x] ) {
	  ese_nodo = true;
	}
      }
      if( !ese_nodo ) {
	return false;
      }
    }

    return true;
}

uint CliqueMaximaFronteraExacto::calcularFrontera ( vuint& cliqueNodos, vbool& cliqueBool) {
  uint count = 0;
  for(uint x = 0; x < cliqueNodos.size(); x++) {
      vuint vecindad = this->grafo.vecindad(cliqueNodos[x]);
      for(uint y = 0; y < vecindad.size(); y++) {
	  if( !cliqueBool[vecindad[y]]) {
	    count++;
	  }
      }
  }
  return count;
}

vuint CliqueMaximaFronteraExacto::obtenerAdyacentes( vuint& clique, vbool& cliqueBool ) {
  vuint res;
 
  if( clique.size() == 0 ) {
      for(uint j = 0; j < this->grafo.nodos(); j++) {
	res.push_back(j);
      }
      return res;
  }
  vbool vecBool(cBool);
  for(uint i = 0; i < clique.size(); i++) { 
    vuint adyacentes = this->grafo.vecindad(clique[i]);
    for(uint x = 0; x < adyacentes.size(); x++) {
	if( !vecBool[adyacentes[x]] ) {
	    res.push_back(adyacentes[x]);    
	    vecBool[adyacentes[x]] = true; 
	}
    }
  }
  
  for(uint x = 0; x < res.size(); x++) {
    if( cliqueBool[res[x]] ) { 
      res.erase(res.begin() + x);
    }
  }
  return res;
}

CliqueMaximaFronteraExacto::Clique CliqueMaximaFronteraExacto::backtracking( vuint clique, vbool cliqueBool, CliqueMaximaFronteraExacto::Clique solucion ) {
    vuint adyacentes = obtenerAdyacentes(clique, cliqueBool);
    
    for(uint i = 0; i < adyacentes.size(); i++) {
      bool sonClique = formaClique(adyacentes[i], clique);    
      if( sonClique ) {
	uint frontera = 0;
	clique.push_back(adyacentes[i]);
	cliqueBool[adyacentes[i]] = true;
	frontera = calcularFrontera(clique, cliqueBool);
	
	if( frontera > solucion.frontera ) {
	  solucion.nodos = clique;
	  solucion.frontera = frontera;
	}
	
	solucion = backtracking( clique, cliqueBool, solucion );
	cliqueBool[adyacentes[i]] = false;
	clique.pop_back();
      }
    }
    return solucion;
}


vuint CliqueMaximaFronteraExacto::resolver(uint& frontera) 
{
	// Rellenar con la solución
	// Pueden acceder a la variable grafo definida en CliqueMaximaFrontera: this->grafo	
	frontera = 0;
	vuint nodos(this->grafo.nodos());

	vbool cliqueBoolTmp;
	for(uint i = 0; i < nodos.size(); i++) {
	  cliqueBoolTmp.push_back(false);
	}
	cBool = vbool(cliqueBoolTmp);
	
	vuint clique;
	CliqueMaximaFronteraExacto::Clique solucion;
	solucion.nodos = vuint();
	solucion.frontera = 0;	
	solucion = backtracking(clique, cliqueBoolTmp, solucion);

	frontera = solucion.frontera;
	return solucion.nodos;
}

