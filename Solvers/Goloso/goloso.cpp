# include "goloso.h"

CliqueMaximaFronteraGoloso::CliqueMaximaFronteraGoloso(const Grafo& g) : CliqueMaximaFrontera(g) 
{	
}

vuint CliqueMaximaFronteraGoloso::resolver(uint& frontera) 
{
	uint nodoMejorGrado = 0;
	uint mejorFrontera = 0;

	for (uint i = 0; i < this->grafo.nodos(); i++)
	{
		uint grado = this->grafo.vecindad(i).size();
		if (grado > mejorFrontera)
		{
			nodoMejorGrado = i;
			mejorFrontera = grado;
		}
	}

	vbool enSolucion(this->grafo.nodos());

	enSolucion[nodoMejorGrado] = true;
	uint nodosEnSolucion = 1;
	uint fronteraSolucion = mejorFrontera;

	while (true)
	{
		bool mejore = false;
		nodoMejorGrado = 0;
		mejorFrontera = fronteraSolucion; // Es goloso, y quiero mejorar la frontera que ya tengo, no elegir el mejor de los que me queda

		for (uint i = 0; i < this->grafo.nodos(); i++)
		{
			if (!enSolucion[i] && this->EsCliqueAgregando(i, enSolucion))
			{
				uint nuevaFrontera = this->RecalcularFronteraAgregando(i, fronteraSolucion, nodosEnSolucion);

				if (nuevaFrontera > mejorFrontera)
				{
					nodoMejorGrado = i;
					mejorFrontera = nuevaFrontera;
					mejore = true;
				}
			}
		}

		if (mejore)
		{
			nodosEnSolucion++;
			fronteraSolucion = mejorFrontera;
			enSolucion[nodoMejorGrado] = true;
		}
		else
		{
			break;
		}
	}

	vuint solucion;

	for (uint i = 0; i < this->grafo.nodos(); i++)
	{
		if (enSolucion[i])
		{
			solucion.push_back(i);
		}
	}

	frontera = fronteraSolucion;
	return solucion;
}