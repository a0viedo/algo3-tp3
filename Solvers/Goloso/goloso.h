#ifndef __AED3__GOLOSO__
#define __AED3__GOLOSO__

#include "../cmf.h"

class CliqueMaximaFronteraGoloso : public CliqueMaximaFrontera 
{
public:
	CliqueMaximaFronteraGoloso(const Grafo& g);
	vuint resolver(uint& frontera);
private:
	// Definir otros métodos que necesiten
};

#endif
