#include <sys/time.h>
#include <string>
#include <fstream>
#include "tinydir.h"
#include "Common/util.h"
#include "Solvers/cmf.h"
#include "Solvers/Exacto/exacto.h"
#include "Solvers/Goloso/goloso.h"
#include "Solvers/BusquedaLocal/busquedalocal.h"
#include "Solvers/BusquedaTabu/busquedatabu.h"

using namespace std;

string getFileExtension (char* filename);
string getPathWithoutExtension (char* path);
vuint leerOutput(istream& is, uint& frontera);

void processTest (string path);

// Funciones y variables globales para la medicion de tiempo de ejecucion
timeval comienzo, fin;
void init_time() {
	gettimeofday(&comienzo, 0);
}

double get_time() {
	gettimeofday(&fin, 0);
	return (1000000 * (fin.tv_sec - comienzo.tv_sec) + (fin.tv_usec - comienzo.tv_usec)) / 1000000.0;
}

int fallados = 0;
int pasados = 0;

int archivoInputNoEncontrado = 0;
int archivoOutputNoEncontrado = 0;

int main() 
{
	tinydir_dir dir;
	tinydir_open(&dir, "./Tests Catedra/");

	while (dir.has_next)
	{
		tinydir_file file;
		tinydir_readfile(&dir, &file);

		if (file.is_dir == 1)
		{
			tinydir_next(&dir);
			continue;
		}

		if (getFileExtension(file.name) == "in")
		{
			string path = getPathWithoutExtension(file.path);
			processTest(path);
		}

		tinydir_next(&dir);
	}

	tinydir_close(&dir);
	
	cout << endl << endl;

	cout << "Tests: " << fallados + pasados << endl << endl;

	cout << "Pasados: " << pasados << endl;
	cout << "Fallados: " << fallados << endl << endl;

	cout << "Fallados con archivo de input no encontrado: " << archivoInputNoEncontrado << endl;
	cout << "Fallados con archivo de output no encontrado: " << archivoOutputNoEncontrado << endl;
	
	return EXIT_SUCCESS;
}

void processTest (string path)
{
	cout << path << " ";

	fstream fs;

	string inputFile = path + ".in";
	string outputFile = path + ".out";

	fs.open(inputFile, std::fstream::in);

	if (!fs.is_open())
	{
		fallados++;
		archivoInputNoEncontrado++;
		cout << "FALLO" << endl;
		return;
	}

	Grafo grafo = leerInput(fs);
	fs.close();

	fs.open(outputFile, std::fstream::in);

	if (!fs.is_open())
	{
		fallados++;
		archivoOutputNoEncontrado++;
		cout << "FALLO" << endl;
		return;
	}

	uint frontera;
	vuint solucion = leerOutput(fs, frontera);
	fs.close();

	uint fronteraObtenida;

// 	CliqueMaximaFronteraGoloso solver(grafo);
	CliqueMaximaFronteraBusquedaLocal solver(grafo);
// 	CliqueMaximaFronteraBusquedaTabu solver(grafo, true, grafo.nodos() * grafo.nodos(), grafo.nodos());
	
	init_time();
	vuint obtenida = solver.resolver(fronteraObtenida);
	double time = get_time()*1000; // En milisegundos
	
	if (frontera == fronteraObtenida)
	{
		bool esSolucion = true;

		for (uint i = 0; i < solucion.size(); i++)
		{
			for (uint j = 0; j < solucion.size(); j++)
			{
				if (i == j)
				{
					continue;
				}

				if (!grafo.sonVecinos(solucion[i], solucion[j]))
				{
					esSolucion = false;
				}
			}
		}

		if (esSolucion)
		{
			pasados++;
			cout << "PASO - Tiempo: " << time << "ms" << endl;
			return;
		}
	}

	long diff = (long)(frontera) - (long)(fronteraObtenida);
	
	if (diff == 0)
	{
		cout << "FALLO" << " - " << "No hay diferencia - Tiempo: " << time << "ms" << endl;
	}
	else
	{
		cout << "FALLO" << " - " << "Diferencia: " << diff << " - Tiempo: " << time << "ms" << endl;
	}
	
	fallados++;
	
}

vuint leerOutput(istream& is, uint& frontera)
{
	uint n;
	is >> frontera >> n;

	vuint solucion(n);

	for (uint k = 0; k < n; k++)
	{
		uint i;
		is >> i;
		i--;
		solucion[k] = i;
	}

	return solucion;
}

string getFileExtension (char* filename)
{
	string extension = "";
	string fullname = filename;
	
	string::iterator iterador = fullname.end();

	while (*(--iterador) != '.')
	{
		extension = *(iterador) + extension;
	}

	return extension;
}

string getPathWithoutExtension (char* path)
{
	string pathWithoutExtension = "";
	string fullpath = path;
	
	string::iterator iterador = fullpath.end();

	while (*(--iterador) != '.')
	{		
	}

	while ((--iterador) != fullpath.begin())
	{		
		pathWithoutExtension = *(iterador) + pathWithoutExtension;
	}

	pathWithoutExtension = "." + pathWithoutExtension;

	return pathWithoutExtension;
}