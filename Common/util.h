#ifndef __AED3__UTIL__
#define __AED3__UTIL__

#include "common.h"
#include "grafo.h"
#include <iostream>

using namespace std;

Grafo leerInput(istream&);
void escribirOutput(ostream&, const int& frontera, const vuint&);

#endif
