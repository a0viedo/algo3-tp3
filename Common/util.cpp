#include "util.h"

Grafo leerInput(istream& is) 
{
	uint n, m;
	is >> n >> m;

	Grafo grafo(n);
	for(uint k = 0; k < m; k++) 
	{
		uint i,j;
		is >> i >> j;
		i--; j--;

		grafo.agregarArista(i, j);
	}

	return grafo;
}

void escribirOutput(ostream& os, const int& frontera, const vuint& solucion) 
{
	os << frontera << " " << solucion.size();

	for(uint i = 0; i < solucion.size(); i++) 
	{
		os << " " << (solucion[i]+1);
	}

	os << endl;
}