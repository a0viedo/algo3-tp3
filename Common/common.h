#ifndef __AED3__COMMON__
#define __AED3__COMMON__

#include <vector>
#include <algorithm>

typedef unsigned int uint;
typedef std::vector<uint> vuint;
typedef std::vector<vuint> vvuint;
typedef std::vector<int> vint;
typedef std::vector<bool> vbool;

#endif
