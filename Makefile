CXX=g++
CXXFLAGS= -O2 -std=c++11 -Wall

OBJS=Common/util.o Solvers/Exacto/exacto.o Solvers/Goloso/goloso.o Solvers/BusquedaLocal/busquedalocal.o Solvers/BusquedaTabu/busquedatabu.o
OBJM= manualTesterMain.o
OBJA=autoTesterMain.o
MANUAL=ManualTester
AUTO=AutoTester

.PHONY: all clean new

all: $(MANUAL) $(AUTO)

$(MANUAL): $(OBJS) $(OBJM)
	$(CXX) $(CXXFLAGS) -o $(MANUAL) $(OBJM) $(OBJS)

$(AUTO): $(OBJS) $(OBJA)
	$(CXX) $(CXXFLAGS) -o $(AUTO) $(OBJA) $(OBJS)

.cpp.o:
	$(CXX) $(CXXFLAGS) -c $<  -o $@
	
clean:
	rm -f $(AUTO) $(MANUAL) $(OBJS) $(OBJM) $(OBJA)
	
new: clean all