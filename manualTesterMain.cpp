#include "Common/util.h"
#include "Solvers/cmf.h"
#include "Solvers/Exacto/exacto.h"
#include "Solvers/Goloso/goloso.h"
#include "Solvers/BusquedaLocal/busquedalocal.h"
#include "Solvers/BusquedaTabu/busquedatabu.h"

int main() 
{		
	Grafo grafo = leerInput(cin);
	
// 	CliqueMaximaFrontera* solver = new CliqueMaximaFronteraBusquedaTabu(grafo, true, grafo.nodos(), grafo.nodos());
	CliqueMaximaFrontera* solver = new CliqueMaximaFronteraBusquedaLocal(grafo);
// 	CliqueMaximaFrontera* solver = new CliqueMaximaFronteraGoloso(grafo);
// 	CliqueMaximaFrontera* solver = new CliqueMaximaFronteraExacto(grafo);

	uint frontera;
	vuint solucion = solver->resolver(frontera);

	escribirOutput(cout, frontera, solucion);

	delete solver;
	
	return EXIT_SUCCESS;
}
